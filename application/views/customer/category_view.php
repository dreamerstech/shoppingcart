<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <?php $this->load->view("customer/layout/header_view"); ?>
    <body>
        <?php $this->load->view("customer/layout/header_content_view"); ?>
        <style>
            .active-product-area{
                padding-top: 200px;
            }
        </style>
        <!-- start product Area -->
        <section class="active-product-area section_gap">
            <!-- single product slide -->
            <div class="single-product-slider">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <div class="section-title">
                                <h1>Products</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- single product -->
                        <?php foreach ($productList as $key => $product) { ?>
                            <div class="col-lg-3 col-md-6">
                                <a href="<?= base_url("productdetails/") . $product['id']; ?>">
                                    <div class="single-product">
                                        <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/product/p1.jpg" alt="">
                                        <div class="product-details">
                                            <h6><?= $product['name']; ?></h6>
                                            <div class="price">
                                                <h6>$ <?= number_format($product['price'], 2); ?></h6>
                                            </div>
<!--                                            <div class="prd-bottom">
                                                <a class="primary-btn" href="javascript:void(0);" onclick="addTOCart('<?= $product['id']; ?>', '<?= $product['name']; ?>',1)">Add to Cart</a>
                                            </div>-->
                                        </div>
                                    </div>
                                </a>
                            </div>                            
                        <?php } ?>
                    </div>
                </div>
            </div>  
        </section>
        <!-- end product Area -->


        <?php $this->load->view("customer/layout/footer_content_view"); ?>
        <?php $this->load->view("customer/layout/footer_view"); ?>
    </body>
</html>