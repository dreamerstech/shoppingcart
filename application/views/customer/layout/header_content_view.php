
<!-- Start Header Area -->
<header class="header_area sticky-header">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light main_box">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="<?= base_url(); ?>"><img src="<?= base_url('assets/customer/'); ?>img/logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item active"><a class="nav-link" href="<?= base_url(); ?>">Home</a></li>
                        <li class="nav-item submenu dropdown"><a class="nav-link" href="<?= base_url('categories'); ?>">Shop Category</a></li>
                        <?php if (isset($_SESSION['name'])) { ?>
                        <li class="nav-item submenu dropdown"><a class="nav-link" href="javascript:void(0)"><?= $_SESSION['name'] ?></a></li>
                            <li class="nav-item submenu dropdown"><a class="nav-link" href="<?= base_url('login/logout'); ?>">Logout</a></li>
                        <?php } else { ?>
                            <li class="nav-item submenu dropdown"><a class="nav-link" href="<?= base_url('login'); ?>">Login</a></li>
                        <?php } ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item"><a href="javascript:void(0)" onclick="checkout();" class="cart"><span id="shoppingCartNotification" class="ti-shopping-cart"></span></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!-- End Header Area -->