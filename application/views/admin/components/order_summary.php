<?php
$modeOfPaymentArr = MODE_OF_PAYMENT;
$orderStatusArr = ORDER_STATUS;
?>
<div class="modal fade" id="modalOrderSummary" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Order Summary</h4>
            </div>
            <div class="modal-body">
                <div class="box box-solid">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <dl>
                            <dt>Products</dt>
                            <table class="table">
                                <tr>
                                    <th>Product Name</th>
                                    <th>Price/Unit</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                <?php
                                $billTotal = 0;
                                foreach ($orderSummary['productData'] as $key => $product) { 
                                    $productTotal = floatval($product['quantity']) * floatval($product['price']);
                                    $billTotal+= $productTotal;
                                    ?>
                                    <tr>
                                        <td><?= $product['product_name'] ?></td>
                                        <td><?= number_format($product['price'],2); ?></td>
                                        <td> x <?= $product['quantity'] ?></td>
                                        <td> <?= number_format($productTotal, 2); ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                            <div class="row">
                                <div class="col-md-6">
                                    <dt>Billing Address</dt>
                                    <dd><?= $orderSummary['first_name'] . " " . $orderSummary['last_name'] . "<br>" . $orderSummary['phone_number'] . "<br>" . $orderSummary['email_id'] . "<br>"; ?></dd>
                                    <dd><?= $orderSummary['address'] . "<br>" . $orderSummary['city'] . "<br>" . $orderSummary['district']; ?></dd>
                                </div>
                                <div class="col-md-6">
                                    <dt>Mode of Payment</dt>
                                    <dd><?= $modeOfPaymentArr[$orderSummary['mode_of_payment']]; ?></dd>
                                    <dt>Order Status</dt>
                                    <dd><?= $orderStatusArr[$orderSummary['status']]; ?></dd>
                                    <dt>Total Bill</dt>
                                    <dd><?= number_format($billTotal, 2); ?></dd>
                                </div>
                            </div>
                        </dl>
                    </div>
                    <!-- /.box-body -->
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>