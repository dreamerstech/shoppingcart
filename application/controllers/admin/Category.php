<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }
    
    public function index() {
        $this->load->view('admin/category_view');
    }

    public function getCategoryList() {
        $this->load->model("admin/Category_Model");
        $data['categoryList'] = $this->Category_Model->getCategoryData();
        $this->load->view("admin/components/category_list", $data);
    }

    public function addCategory() {
        $this->load->view("admin/components/category_add");
    }

    public function addSaveCategory() {
        $dataRule = array(
            array('field' => 'name', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $insertData['name'] = $this->input->post("name");
            $insertData['created_ts'] = gmdate("Y-m-d H:i:s");
            $insertData['updated_ts'] = gmdate("Y-m-d H:i:s");
            $this->load->model("admin/Category_Model");
            $response['status'] = $this->Category_Model->addCategory($insertData);
        } else {
            $response['status'] = 'FAIL';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    public function editcategory() {
        $dataRule = array(
            array('field' => 'categoryId', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) { 
        $this->load->model("admin/Category_Model");
        $data['id'] = $whereData['id'] = $this->input->post("categoryId");
        $data['categoryData'] = $this->Category_Model->getCategoryData($whereData)[0];
        $this->load->view("admin/components/category_edit",$data);
        }       
    }

    public function updateCategory() {
        $dataRule = array(
            array('field' => 'id', 'rules' => 'required'),
            array('field' => 'name', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $whereData['id'] = $this->input->post("id");
            $updateData['name'] = $this->input->post("name");
            $updateData['updated_ts'] = gmdate("Y-m-d H:i:s");
            $this->load->model("admin/Category_Model");
            $response['status'] = $this->Category_Model->updateCategory($updateData, $whereData);
        } else {
            $response['status'] = 'FAIL';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    public function deleteCategory() {
        $dataRule = array(
            array('field' => 'categoryId', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $whereData['id'] = $this->input->post("categoryId");
            $this->load->model("admin/Category_Model");
            $response['status'] = $this->Category_Model->deleteCategory($whereData);
        } else {
            $response['status'] = 'FAIL';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

}
