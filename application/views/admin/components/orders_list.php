<?php
$cnt = 0;
if (count($orderList) != 0) {
    $method = MODE_OF_PAYMENT;
    $status = ORDER_STATUS;
    ?>
    <table class="table table-striped">
        <tr>
            <th>Sl. No.</th>
            <th>Customer Name</th>
            <th>Product(s)</th>
            <th>Mode of Payment</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        <?php foreach ($orderList as $key => $order) { ?>
        <tr>
            <td><?= $cnt+=1; ?></td>
            <td><?= $order['username']; ?></td>
            <td><?= $order['product_names']; ?></td>
            <td><?= $method[$order['mode_of_payment']]; ?></td>
            <td><?= $status[$order['status']]; ?></td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" onclick="viewOrderSummary('<?= $order['orderId']; ?>')"><i class="fa fa-book"></i></button>
                </div>
            </td>
        </tr>
        <?php } ?>
    </table>
<?php } else { ?>
    <div class="well well-sm nodata">No data found.</div>
    <?php
}
?>