<?php
$cnt = 0;
if (count($productList) != 0) {
    ?>
    <table class="table table-striped">
        <tr>
            <th>Sl. No.</th>
            <th>Name</th>
            <th>Category</th>
            <th>Price</th>
            <th>Action</th>
        </tr>
        <?php foreach ($productList as $key => $product) { ?>
        <tr>
            <td><?= $cnt+=1; ?></td>
            <td><?= $product['name']; ?></td>
            <td><?= $product['category_name']; ?></td>
            <td><?= $product['price']; ?></td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" onclick="editProduct('<?= $product['id']; ?>')"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-default btn-flat" onclick="deleteProduct('<?= $product['id']; ?>')"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <?php } ?>
    </table>
<?php } else { ?>
    <div class="well well-sm nodata">No data found.</div>
    <?php
}
?>