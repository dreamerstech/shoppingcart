<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index() {
        $this->load->view('login_view');
    }

    public function validate_login() {
        $response = [];
        $dataRule = array(
            array('field' => 'username', 'rules' => 'required'),
            array('field' => 'password', 'rules' => 'required|min_length[5]|max_length[15]'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $where_data['username'] = $this->input->post('username');
            $password = $this->input->post('password');
            $this->load->model('Login_model');
            $user_data = $this->Login_model->validate_login($where_data);
            if (!empty($user_data)) {
                if (password_verify($password, $user_data['password'])) {
                    $_SESSION['name'] = $user_data['name'];
                    $_SESSION['user_id'] = $user_data['id'];
                    $_SESSION['user_type'] = $user_data['user_type'];
                    $_SESSION['SESSION_KEY'] = md5(rand(123456, 999999) . time());
                    $response['user_type'] = $_SESSION['user_type'];
                    $response['status'] = 'SUCCESS';

                    $insert_data['user_id'] = $_SESSION['user_id'];
                    $insert_data['session_id'] = $_SESSION['SESSION_KEY'];
                    $insert_data['login_at'] = date("Y-m-d H:i:s");
                    $user_data = $this->Login_model->log_login_history($insert_data);
                    $this->db->insert("login_history", $insert_data);
                } else {
                    $response['status'] = 'FAIL';
                }
            } else {
                $response['status'] = 'FAIL';
            }
        } else {
            $response['status'] = 'FAIL';
            $response['error'] = 'Input validation failed.';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    function forgot_password() {
        $this->load->view('forgot_password_view');
    }

    function send_reset_link() {
        $response = [];
        $dataRule = array(
            array('field' => 'email_id', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $where_data['email_id'] = $this->input->post('email_id');
            $this->load->model('Login_model');
            $response = $this->Login_model->send_reset_link($where_data);
        } else {
            $response['status'] = 'FAIL';
            $response['error'] = 'Input validation failed.';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    function reset_password($token) {
        $where_data['reset_token'] = $token;
        $this->load->model('Login_model');
        $data = $this->Login_model->validate_reset_token($where_data);
        $data['reset_token'] = $token;
        if ($data) {
            $this->load->view('reset_password_view', $data);
        } else {
            $this->index();
        }
    }

    function reset_save_password() {

        $response = [];
        $dataRule = array(
            array('field' => 'hid_reset_token', 'rules' => 'required'),
            array('field' => 'new_password', 'rules' => 'required|min_length[5]|max_length[15]'),
            array('field' => 'confirm_password', 'rules' => 'trim|required|matches[new_password]'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $whereData['reset_token'] = $this->input->post('hid_reset_token');
            $new_password = $this->input->post('new_password');
            $confirm_password = $this->input->post('confirm_password');
            if ($new_password === $confirm_password) {
                $this->load->model('Login_model');
                $updateData['password'] = $this->encrypt_password($new_password);
                $result['status'] = $this->Login_model->reset_save_password($updateData, $whereData);
            }
        } else {
            $response['status'] = 'FAIL';
            $response['error'] = 'Input validation failed.';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    public function encrypt_password($password) {
        $options = ['cost' => 12,];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }    

    public function logout() {
        session_destroy();
        header("Location:" . base_url() . "login");
    }

}
