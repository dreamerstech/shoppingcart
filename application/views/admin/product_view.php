<!DOCTYPE html>
<html>
    <?php $this->load->view('admin/layout/header'); ?>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <?php $this->load->view('admin/layout/header-content'); ?>
            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <?php $this->load->view('admin/layout/sidebar'); ?>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Product
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Product</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product</h3>
                            <button class="btn btn-primary pull-right" onclick="addProduct();"><i class="fa fa-plus-circle"></i> Add</button>                            
                        </div>
                        <div class="box-body">
                            <div id="dataContent"></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <?php $this->load->view('admin/layout/footer-content'); ?>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <?php $this->load->view('admin/layout/footer'); ?>
        <div id="modalContainer"></div>
    </body>
<script>

        $(document).ready(function () {
            listProduct();
        });

        function listProduct() {
            $.post($("#hid_site_url").val() + "admin/product/getProductData", function (response) {
                $("#dataContent").html(response);
            });
        }

        function addProduct() {
            $.post($("#hid_site_url").val() + "admin/product/addproduct", function (response) {
                $("#modalContainer").html(response);
                $("#modalAddProduct").modal("show");
            });
        }

        function addSaveProduct() {
            $("#formAddProduct").validate();
            if ($("#formAddProduct").valid() == true) {
                $.post($("#hid_site_url").val() + "admin/product/addsaveproduct", $("#formAddProduct").serialize(), function (response) {
                    if (response.status == 'SUCCESS') {
                        $.notify('Product added successfully', 'success');
                        listProduct();
                        $("#modalAddProduct").modal("hide");
                    } else {
                        $.notify('Error adding product', 'error');
                    }
                });
            }
        }

        function editProduct(id) {
            $.post($("#hid_site_url").val() + "admin/product/editproduct", {productId: id}, function (response) {
                $("#modalContainer").html(response);
                $("#modalEditProduct").modal("show");
            });
        }

        function updateProduct() {
            $.post($("#hid_site_url").val() + "admin/product/updateProduct", $("#formEditProduct").serialize(), function (response) {
                if (response.status == 'SUCCESS') {
                    $.notify('Product updated successfully', 'success');
                    listProduct();
                    $("#modalEditProduct").modal("hide");
                } else {
                    $.notify('Error updating product', 'error');
                }
            });
        }

        function deleteProduct(id) {
            $.post($("#hid_site_url").val() + "admin/product/deleteProduct", {productId: id}, function (response) {
                if (response.status == 'SUCCESS') {
                    $.notify('Product deleted successfully', 'success');
                    listProduct();
                } else {
                    $.notify('Error deleting product', 'error');
                }
            });
        }

    </script>    
</html>
