<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function productDetails($productId) {
        if (!empty($productId)) {
            $whereData['p.id'] = $productId;
            $this->load->model("admin/Product_Model");
            $data['productData'] = $this->Product_Model->getProductData($whereData)[0];
            $this->load->view('customer/productDetails_view',$data);
        }
    }

}

?>