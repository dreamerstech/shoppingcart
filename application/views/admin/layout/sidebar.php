<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="height: 45px;">
            <div class="pull-left image">
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['name']; ?></p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?php echo base_url('dashboard'); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/category'); ?>">
                    <i class="fa fa-users"></i> <span>Category</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/product'); ?>">
                    <i class="fa fa-book"></i> <span>Product</span>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('admin/order'); ?>">
                    <i class="fa fa-book"></i> <span>Orders</span>
                </a>
            </li>
    <!--        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
