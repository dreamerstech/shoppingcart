<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $this->load->model("admin/Category_Model");
        $data['categoryList'] = $this->Category_Model->getCategoryData();
        $this->load->view('customer/home_view');
    }

}

?>