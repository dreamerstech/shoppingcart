<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Category_Model extends CI_Model {

    public function getCategoryData($wheredata = []) {
        $wheredata['is_active'] = 1;
        $this->db->select("id,name");
        if ($result = $this->db->get_where("master_category",$wheredata)) {
            return $result->result('array');
        } else {
            return [];
        }
    }

    public function addCategory($insertData) {
        if ($this->db->insert("master_category", $insertData)) {
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }

    public function updateCategory($updateData, $whereData) {
        if ($this->db->update("master_category", $updateData, $whereData)) {
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }

    public function deleteCategory($whereData) {
        if ($this->db->delete("master_category", $whereData)) {
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }

}
