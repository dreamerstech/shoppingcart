<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index() {
        $this->load->view('admin/product_view');
    }

    public function getProductData() {
        $this->load->model("admin/Product_Model");
        $data['productList'] = $this->Product_Model->getProductData();
        $this->load->view("admin/components/product_list", $data);
    }

    public function addProduct() {
        $this->load->model("admin/Category_Model");
        $data['categoryList'] = $this->Category_Model->getCategoryData();
        $this->load->view("admin/components/product_add", $data);
    }

    public function addSaveProduct() {
        $dataRule = array(
            array('field' => 'name', 'rules' => 'required'),
            array('field' => 'category_id', 'rules' => 'required'),
            array('field' => 'price', 'rules' => 'required|numeric'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $insertData['name'] = $this->input->post("name");
            $insertData['category_id'] = $this->input->post("category_id");
            $insertData['price'] = $this->input->post("price");
            $insertData['in_stock'] = !empty($this->input->post("in_stock"))?$this->input->post("in_stock"):0;
            $insertData['created_ts'] = gmdate("Y-m-d H:i:s");
            $insertData['updated_ts'] = gmdate("Y-m-d H:i:s");
            $this->load->model("admin/Product_Model");
            $response['status'] = $this->Product_Model->addProduct($insertData);
        } else {
            $response['status'] = 'FAIL';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    public function editProduct() {
        $dataRule = array(
            array('field' => 'productId', 'rules' => 'required|numeric'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $this->load->model("admin/Category_Model");
            $data['id'] = $this->input->post("productId");
            $data['categoryList'] = $this->Category_Model->getCategoryData();

            $whereData['p.id'] = $this->input->post("productId");
            $this->load->model("admin/Product_Model");
            $data['productData'] = $this->Product_Model->getProductData($whereData)[0];
            $this->load->view("admin/components/product_edit", $data);
        }
    }

    public function updateProduct() {
        $dataRule = array(
            array('field' => 'productId', 'rules' => 'required'),
            array('field' => 'name', 'rules' => 'required'),
            array('field' => 'category_id', 'rules' => 'required'),
            array('field' => 'price', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $whereData['id'] = $this->input->post("productId");
            $updateData['name'] = $this->input->post("name");
            $updateData['category_id'] = $this->input->post("category_id");
            $updateData['price'] = $this->input->post("price");
            $updateData['in_stock'] = $this->input->post("in_stock");
            $updateData['updated_ts'] = gmdate("Y-m-d H:i:s");
            $this->load->model("admin/Product_Model");
            $response['status'] = $this->Product_Model->updateProduct($updateData, $whereData);
        } else {
            $response['status'] = 'FAIL';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

    public function deleteProduct() {
        $dataRule = array(
            array('field' => 'productId', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $whereData['id'] = $this->input->post("productId");
            $this->load->model("admin/Product_Model");
            $response['status'] = $this->Product_Model->deleteProduct($whereData);
        } else {
            $response['status'] = 'FAIL';
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

}
