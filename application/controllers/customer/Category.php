<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function index() {
        $this->load->model("admin/Category_Model");
        $data['categoryList'] = $this->Category_Model->getCategoryData(["is_active" => 1]);
        $this->load->view('customer/home_view',$data);
    }
    
    public function listProducts($category_id) {
        if(!empty($category_id)){
        $this->load->model("admin/Product_Model");
        $data['productList'] = $this->Product_Model->getProductData(["category_id"=>$category_id]);
        $this->load->view("customer/category_view", $data);
            
        }
    }

}

?>