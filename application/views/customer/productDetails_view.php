<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <?php $this->load->view("customer/layout/header_view"); ?>
    <body>
        <?php $this->load->view("customer/layout/header_content_view"); ?>

        <div class="product_image_area" style="padding-top: 200px;padding-bottom: 200px;">
            <div class="container">
                <div class="row s_product_inner">
                    <div class="col-lg-6">
                        <div class="s_Product_carousel owl-carousel owl-theme owl-loaded">

                            <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1080px, 0px, 0px); transition: all 0s ease 0s; width: 3780px;"><div class="owl-item cloned" style="width: 540px; margin-right: 0px;"><div class="single-prd-item">
                                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/category/s-p1.jpg" alt="">
                                        </div></div><div class="owl-item cloned" style="width: 540px; margin-right: 0px;"><div class="single-prd-item">
                                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/category/s-p1.jpg" alt="">
                                        </div></div><div class="owl-item active" style="width: 540px; margin-right: 0px;"><div class="single-prd-item">
                                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/category/s-p1.jpg" alt="">
                                        </div></div><div class="owl-item" style="width: 540px; margin-right: 0px;"><div class="single-prd-item">
                                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/category/s-p1.jpg" alt="">
                                        </div></div><div class="owl-item" style="width: 540px; margin-right: 0px;"><div class="single-prd-item">
                                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/category/s-p1.jpg" alt="">
                                        </div></div><div class="owl-item cloned" style="width: 540px; margin-right: 0px;"><div class="single-prd-item">
                                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/category/s-p1.jpg" alt="">
                                        </div></div><div class="owl-item cloned" style="width: 540px; margin-right: 0px;"><div class="single-prd-item">
                                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/category/s-p1.jpg" alt="">
                                        </div></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style="display: none;">prev</div><div class="owl-next" style="display: none;">next</div></div><div class="owl-dots" style=""><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div></div></div></div>
                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <div class="s_product_text">
                            <h3><?= $productData['name']; ?></h3>
                            <h2>$<?= $productData['price']; ?></h2>
                            <ul class="list">
                                <li><a class="active" href="#"><span>Category</span> : <?= $productData['category_name']; ?></a></li>
                                <li><a href="#"><span>Availibility</span> : <?= ($productData['in_stock'] == 1) ? "In Stock" : "Out of Stock"; ?></a></li>
                            </ul>
                            <p>Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for
                                something that can make your interior look awesome, and at the same time give you the pleasant warm feeling
                                during the winter.</p>
                            <div class="product_count">
                                <label for="qty">Quantity:</label>
                                <input type="text" name="productQuantity" id="productQuantity" maxlength="12" value="1" title="Quantity:" class="input-text qty">
                                <button onclick="var result = document.getElementById('productQuantity'); var productQuantity = result.value; if (!isNaN(productQuantity))
                                            result.value++;
                                        return false;" class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                                <button onclick="var result = document.getElementById('productQuantity'); var productQuantity = result.value; if (!isNaN(productQuantity) && productQuantity > 1) result.value--; return false;" class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button>
                            </div>
                            <div class="card_area d-flex align-items-center" style="margin-bottom: 10px;">
                                <?php if ($productData['in_stock'] == 1) { ?>
                                    <a class="primary-btn" href="javascript:void(0);" onclick="addTOCart('<?= $productData['id']; ?>', '<?= $productData['name']; ?>')">Add to Cart</a>
                                <?php } else { ?>
                                    <a class="primary-btn" href="javascript:void(0);" >Notify Me</a>
<?php } ?>
                            </div>
                            <div id="productToCartNotification" class="alert"></div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view("customer/layout/footer_content_view"); ?>
<?php $this->load->view("customer/layout/footer_view"); ?>
    </body>
</html>