-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2019 at 12:35 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoppingcart`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

DROP TABLE IF EXISTS `login_history`;
CREATE TABLE `login_history` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `session_id` varchar(250) NOT NULL,
  `login_at` datetime NOT NULL,
  `logout_at` datetime DEFAULT NULL,
  `remember_login` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`id`, `user_id`, `session_id`, `login_at`, `logout_at`, `remember_login`) VALUES
(1, 1, 'd94e0a42fdc4bed6b4ddb8cf4cdcc85d', '2019-07-23 22:52:53', NULL, 0),
(2, 1, 'd94e0a42fdc4bed6b4ddb8cf4cdcc85d', '2019-07-23 22:52:53', NULL, 0),
(3, 1, 'a0c065707cffa1ebbf0389b6c632a0cc', '2019-07-23 22:54:48', NULL, 0),
(4, 1, 'a0c065707cffa1ebbf0389b6c632a0cc', '2019-07-23 22:54:48', NULL, 0),
(5, 1, '9d089d0afb80617b72d9ac315843569f', '2019-07-23 22:55:21', NULL, 0),
(6, 1, '9d089d0afb80617b72d9ac315843569f', '2019-07-23 22:55:21', NULL, 0),
(7, 1, '31cd7276c211cfa1cff66d03e6fce96c', '2019-07-23 23:02:05', NULL, 0),
(8, 1, '31cd7276c211cfa1cff66d03e6fce96c', '2019-07-23 23:02:05', NULL, 0),
(9, 2, 'b399e52522e28cab94de457485847ad2', '2019-07-24 21:19:09', NULL, 0),
(10, 2, 'b399e52522e28cab94de457485847ad2', '2019-07-24 21:19:09', NULL, 0),
(11, 2, '01e3ff085104663c902f6551e9562685', '2019-07-24 21:22:49', NULL, 0),
(12, 2, '01e3ff085104663c902f6551e9562685', '2019-07-24 21:22:49', NULL, 0),
(13, 2, '5033e19c673da8365fdb14cbcc332fba', '2019-07-25 12:40:07', NULL, 0),
(14, 2, '5033e19c673da8365fdb14cbcc332fba', '2019-07-25 12:40:07', NULL, 0),
(15, 2, '58e15e77ae5e4b37a641265b5dd3f829', '2019-07-26 14:23:12', NULL, 0),
(16, 2, '58e15e77ae5e4b37a641265b5dd3f829', '2019-07-26 14:23:12', NULL, 0),
(17, 2, 'fc568970b745122f9a0b3a4e6b9630cf', '2019-07-27 01:10:49', NULL, 0),
(18, 2, 'fc568970b745122f9a0b3a4e6b9630cf', '2019-07-27 01:10:49', NULL, 0),
(19, 1, 'e3260c44f1d0e6f0d20ec0e48631309b', '2019-07-28 07:14:38', NULL, 0),
(20, 1, 'e3260c44f1d0e6f0d20ec0e48631309b', '2019-07-28 07:14:38', NULL, 0),
(21, 1, '8173328c81e343d77f3cfc548746e37b', '2019-07-28 07:16:57', NULL, 0),
(22, 1, '8173328c81e343d77f3cfc548746e37b', '2019-07-28 07:16:57', NULL, 0),
(23, 1, 'f7c944d15bb7cac599b1809b02dee0d4', '2019-07-28 22:46:56', NULL, 0),
(24, 1, 'f7c944d15bb7cac599b1809b02dee0d4', '2019-07-28 22:46:56', NULL, 0),
(25, 1, '578d8193b82eece31ee1fd6a860de6cb', '2019-07-28 22:48:15', NULL, 0),
(26, 1, '578d8193b82eece31ee1fd6a860de6cb', '2019-07-28 22:48:15', NULL, 0),
(27, 1, '0a68a25ebe808332127613b6366b2021', '2019-07-28 22:52:52', NULL, 0),
(28, 1, '0a68a25ebe808332127613b6366b2021', '2019-07-28 22:52:52', NULL, 0),
(29, 1, '251dd142b5b44375d3330e9f154a4395', '2019-07-29 20:41:50', NULL, 0),
(30, 1, '251dd142b5b44375d3330e9f154a4395', '2019-07-29 20:41:50', NULL, 0),
(31, 1, 'c93bbe6edfffaf72e9ffa80d8eed389a', '2019-07-29 22:19:00', NULL, 0),
(32, 1, 'c93bbe6edfffaf72e9ffa80d8eed389a', '2019-07-29 22:19:00', NULL, 0),
(33, 1, 'ddefb662441ff6805d6d68eb77f8e2eb', '2019-07-29 22:29:27', NULL, 0),
(34, 1, 'ddefb662441ff6805d6d68eb77f8e2eb', '2019-07-29 22:29:27', NULL, 0),
(35, 2, 'e7d84c52eac2905ce3933f0c18c021d9', '2019-07-29 22:54:53', NULL, 0),
(36, 2, 'e7d84c52eac2905ce3933f0c18c021d9', '2019-07-29 22:54:53', NULL, 0),
(37, 2, 'bef4b145bead5a95b970e530d9fdfb02', '2019-07-30 11:39:38', NULL, 0),
(38, 2, 'bef4b145bead5a95b970e530d9fdfb02', '2019-07-30 11:39:38', NULL, 0),
(39, 2, 'f4c79373256ec1c03230f98915ff1de0', '2019-07-30 12:02:25', NULL, 0),
(40, 2, 'f4c79373256ec1c03230f98915ff1de0', '2019-07-30 12:02:25', NULL, 0),
(41, 1, '7ce31ac06fd3297c6c54b986f3f94556', '2019-07-30 12:12:41', NULL, 0),
(42, 1, '7ce31ac06fd3297c6c54b986f3f94556', '2019-07-30 12:12:41', NULL, 0),
(43, 2, 'c1eece71859b3a068b4aef8ea7a0eb79', '2019-07-30 12:13:55', NULL, 0),
(44, 2, 'c1eece71859b3a068b4aef8ea7a0eb79', '2019-07-30 12:13:55', NULL, 0),
(45, 1, 'ff10f2b124efed6fa1183274f6d19adb', '2019-07-30 12:17:03', NULL, 0),
(46, 1, 'ff10f2b124efed6fa1183274f6d19adb', '2019-07-30 12:17:03', NULL, 0),
(47, 2, '23cbcc32b7c57dcdc752a9f0c16b8022', '2019-07-30 12:19:03', NULL, 0),
(48, 2, '23cbcc32b7c57dcdc752a9f0c16b8022', '2019-07-30 12:19:03', NULL, 0),
(49, 1, '073eb698c54a17a190e6805d63e744cc', '2019-07-30 12:19:24', NULL, 0),
(50, 1, '073eb698c54a17a190e6805d63e744cc', '2019-07-30 12:19:24', NULL, 0),
(51, 2, 'eb6a472a6d99da1a8ed8997f8cae7239', '2019-07-30 12:28:36', NULL, 0),
(52, 2, 'eb6a472a6d99da1a8ed8997f8cae7239', '2019-07-30 12:28:36', NULL, 0),
(53, 2, 'a6593e4b18feae1e7dea2d10b4ceb375', '2019-07-30 12:32:41', NULL, 0),
(54, 2, 'a6593e4b18feae1e7dea2d10b4ceb375', '2019-07-30 12:32:41', NULL, 0),
(55, 1, '192760c3e769a1d802747bf7a73d1b53', '2019-07-30 12:33:48', NULL, 0),
(56, 1, '192760c3e769a1d802747bf7a73d1b53', '2019-07-30 12:33:48', NULL, 0),
(57, 2, 'ac907627eb2fd0dd3132d163183bd39b', '2019-07-30 12:35:04', NULL, 0),
(58, 2, 'ac907627eb2fd0dd3132d163183bd39b', '2019-07-30 12:35:04', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_category`
--

DROP TABLE IF EXISTS `master_category`;
CREATE TABLE `master_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_ts` datetime NOT NULL,
  `updated_ts` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_category`
--

INSERT INTO `master_category` (`id`, `name`, `is_active`, `created_ts`, `updated_ts`) VALUES
(1, 'Category1', 1, '2019-07-30 10:28:47', '2019-07-30 10:28:47'),
(2, 'Category2', 1, '2019-07-30 10:28:53', '2019-07-30 10:28:53'),
(3, 'Category3', 1, '2019-07-30 10:28:59', '2019-07-30 10:28:59');

-- --------------------------------------------------------

--
-- Table structure for table `master_product`
--

DROP TABLE IF EXISTS `master_product`;
CREATE TABLE `master_product` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `category_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `created_ts` datetime NOT NULL,
  `updated_ts` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_product`
--

INSERT INTO `master_product` (`id`, `name`, `category_id`, `price`, `in_stock`, `created_ts`, `updated_ts`) VALUES
(1, 'Product1', 1, '5000.00', 1, '2019-07-30 10:29:17', '2019-07-30 10:29:17'),
(2, 'Product2', 1, '6000.00', 1, '2019-07-30 10:29:29', '2019-07-30 10:29:29'),
(3, 'Product3', 1, '5500.00', 1, '2019-07-30 10:29:47', '2019-07-30 10:29:47'),
(4, 'Product4', 2, '6850.00', 1, '2019-07-30 10:30:03', '2019-07-30 10:30:03'),
(5, 'Product5', 2, '8000.00', 0, '2019-07-30 10:30:13', '2019-07-30 10:30:13'),
(6, 'Product6', 3, '8740.00', 1, '2019-07-30 10:30:29', '2019-07-30 10:30:29'),
(7, 'Product7', 3, '6000.00', 0, '2019-07-30 10:30:46', '2019-07-30 10:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_products`
--

DROP TABLE IF EXISTS `ordered_products`;
CREATE TABLE `ordered_products` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordered_products`
--

INSERT INTO `ordered_products` (`order_id`, `product_id`, `quantity`, `price`) VALUES
(1, 2, 2, '6000.00'),
(1, 4, 1, '6850.00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mode_of_payment` char(1) NOT NULL COMMENT 'C-COD, R-Credit Card, D-Debit Card',
  `status` char(1) NOT NULL DEFAULT 'O' COMMENT 'O-Ordered, A-Accepted, P-Processing, D-Delivered',
  `order_placed_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `mode_of_payment`, `status`, `order_placed_at`) VALUES
(1, 1, 'D', 'O', '2019-07-30 10:34:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `user_type` char(1) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_type`, `email_id`, `username`, `password`, `is_active`, `created_at`) VALUES
(1, 'Praveen', 'C', 'praveen@demo.com', 'praveen', '$2y$12$owCKB0j/AcdCWShEca2kxOoto4V1MQ5DpRq0P0z8npgsiusW8/eDa', 1, '2019-07-24 02:09:10'),
(2, 'Admin', 'A', 'admin@demo.com', 'admin', '$2y$12$owCKB0j/AcdCWShEca2kxOoto4V1MQ5DpRq0P0z8npgsiusW8/eDa', 1, '2019-07-24 02:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `user_billing_address`
--

DROP TABLE IF EXISTS `user_billing_address`;
CREATE TABLE `user_billing_address` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone_number` varchar(12) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(50) NOT NULL,
  `district` varchar(50) NOT NULL,
  `created_ts` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_billing_address`
--

INSERT INTO `user_billing_address` (`id`, `user_id`, `order_id`, `first_name`, `last_name`, `phone_number`, `email_id`, `address`, `city`, `district`, `created_ts`) VALUES
(1, 1, 1, 'Praveen', 'HB', '9876543210', 'praveen@demo.com', '#123, 4th main, 5th Cross, Vijayanagar', 'Bangalore', 'Bangalore urban', '2019-07-30 10:34:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_history`
--
ALTER TABLE `login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_category`
--
ALTER TABLE `master_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_product`
--
ALTER TABLE `master_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id` (`email_id`);

--
-- Indexes for table `user_billing_address`
--
ALTER TABLE `user_billing_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `order_id` (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_history`
--
ALTER TABLE `login_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `master_category`
--
ALTER TABLE `master_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `master_product`
--
ALTER TABLE `master_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_billing_address`
--
ALTER TABLE `user_billing_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `master_product`
--
ALTER TABLE `master_product`
  ADD CONSTRAINT `master_product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `master_category` (`id`);

--
-- Constraints for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD CONSTRAINT `ordered_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `ordered_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `master_product` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_billing_address`
--
ALTER TABLE `user_billing_address`
  ADD CONSTRAINT `user_billing_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_billing_address_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
