<!DOCTYPE html>
<html>
    <?php $this->load->view('admin/layout/header'); ?>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <?php $this->load->view('admin/layout/header-content'); ?>
            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <?php $this->load->view('admin/layout/sidebar'); ?>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Category
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Category</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                Category
                            </h3>
                            <button class="btn btn-primary pull-right" onclick="addCategory();"><i class="fa fa-plus-circle"></i> Add</button>
                        </div>
                        <div class="box-body">
                            <div id="dataContent"></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <?php $this->load->view('admin/layout/footer-content'); ?>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <?php $this->load->view('admin/layout/footer'); ?>
        <div id="modalContainer"></div>
    </body>
    <script>

        $(document).ready(function () {
            listCategory();
        });

        function listCategory() {
            $.post($("#hid_site_url").val() + "admin/category/getcategorylist", function (response) {
                $("#dataContent").html(response);
            });
        }

        function addCategory() {
            $.post($("#hid_site_url").val() + "admin/category/addcategory", function (response) {
                $("#modalContainer").html(response);
                $("#modalAddCategory").modal("show");
            });
        }

        function addSaveCategory() {
            $("#formAddCategory").validate();
            if ($("#formAddCategory").valid() == true) {
                $.post($("#hid_site_url").val() + "admin/category/addsavecategory", $("#formAddCategory").serialize(), function (response) {
                    if (response.status == 'SUCCESS') {
                        $.notify('Category added successfully', 'success');
                        listCategory();
                        $("#modalAddCategory").modal("hide");
                    } else {
                        $.notify('Error adding category', 'error');
                    }
                });
            }
        }

        function editCategory(id) {
            $.post($("#hid_site_url").val() + "admin/category/editcategory", {categoryId: id}, function (response) {
                $("#modalContainer").html(response);
                $("#modalEditCategory").modal("show");
            });
        }

        function updateCategory() {
            $.post($("#hid_site_url").val() + "admin/category/updateCategory", $("#formEditCategory").serialize(), function (response) {
                if (response.status == 'SUCCESS') {
                    $.notify('Category updated successfully', 'success');
                    listCategory();
                    $("#modalEditCategory").modal("hide");
                } else {
                    $.notify('Error updating category', 'error');
                }
            });
        }

        function deleteCategory(id) {
            $.post($("#hid_site_url").val() + "admin/category/deleteCategory", {categoryId: id}, function (response) {
                if (response.status == 'SUCCESS') {
                    $.notify('Category deleted successfully', 'success');
                    listCategory();
                } else {
                    $.notify('Error deleting category', 'error');
                }
            });
        }

    </script>
</html>
