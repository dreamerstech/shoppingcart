<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index() {
        $this->load->view("admin/orders_view");
    }

    public function getOrderList() {
        $this->load->model("admin/Order_Model");
        $data['orderList'] = $this->Order_Model->getOrderList();
        $this->load->view("admin/components/orders_list", $data);
    }

    public function getOrderSummary() {
        $dataRule = array(
            array('field' => 'orderId', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $orderId = $this->input->post("orderId");
            $this->load->model("admin/Order_Model");
            $data['orderSummary'] = $this->Order_Model->getOrderSummary($orderId);
        }
        $this->load->view("admin/components/order_summary", $data);
    }

}

?>