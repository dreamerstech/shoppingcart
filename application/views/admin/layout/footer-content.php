<div id="user_profile_modal_container"></div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018-2019 <a href="https://www.dreamerstech.co.in">Dreamers Technologies</a>.</strong> All rights
    reserved.
</footer>