<?php
$cnt = 0;
if (count($categoryList) != 0) {
    ?>
    <table class="table table-striped">
        <tr>
            <th>Sl. No.</th>
            <th>Name</th>
            <th> Action </th>
        </tr>
        <?php foreach ($categoryList as $key => $category) { ?>
        <tr>
            <td><?= $cnt+=1; ?></td>
            <td><?= $category['name']; ?></td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-flat" onclick="editCategory('<?= $category['id']; ?>')"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-default btn-flat" onclick="deleteCategory('<?= $category['id']; ?>')"><i class="fa fa-trash"></i></button>
                </div>
            </td>
        </tr>
        <?php } ?>
    </table>
<?php } else { ?>
    <div class="well well-sm nodata">No data found.</div>
    <?php
}
?>