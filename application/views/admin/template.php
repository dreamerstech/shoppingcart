<!DOCTYPE html>
<html>
    <?php $this->load->view('admin/layout/header'); ?>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <?php $this->load->view('admin/layout/header-content'); ?>
            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <?php $this->load->view('admin/layout/sidebar'); ?>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Blank page
                        <small>it all starts here</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Examples</a></li>
                        <li class="active">Blank page</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Title</h3>
                        </div>
                        <div class="box-body">
                            
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <?php $this->load->view('admin/layout/footer-content'); ?>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <?php $this->load->view('admin/layout/footer'); ?>
    </body>
</html>
