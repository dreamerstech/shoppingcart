<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login_model extends CI_Model {

    public function validate_login($credentails) {
        $sql = "SELECT id,name, user_type,password FROM users WHERE username='" . $credentails['username'] . "'";
        if($result = $this->db->query($sql)){
            return $result->first_row('array');
        } else {
            return [];
        }
    }
    public function log_login_history($insert_data) {
        if($result = $this->db->insert("login_history",$insert_data)){
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }
    
    public function check_email_address($email_id) {
        $this->db->where('email_id',$email_id);
        if($result = $this->db->get('users')){
            return count($result->result('array'));
        } else{
            return 0;
        }
    }
    public function send_reset_link($where_data) {
        $reset_token = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < 50; $i++) {
            $rand = mt_rand(0, $max);
            $reset_token .= $characters[$rand];
        }

        $update_data['reset_token'] = $reset_token;
        $this->db->update("users", $update_data, ["email_id" => $where_data['email_id']]);

        $reset_link = base_url("login/reset_password/") . $reset_token;

        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_user'] = 'dreamerstechindia@gmail.com';
        $config['smtp_pass'] = 'IndianPrince@123';
        $config['smtp_port'] = '465';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->from('dreamerstechindia@gmail.com', 'DreamersTech');
        $this->email->to($where_data['email_id']);
        $this->email->subject('Password Reset Link');
        $this->email->message('Hi,<br><br> We have recieved a request for password reset and below is the reset link.<br>' . $reset_link);
        $resp = $this->email->send();
        if ($resp) {
            $response['status'] = 'SUCCESS';
            $response['message'] = 'E-mail sent successfully';
        } else {
            $response['status'] = 'FAIL';
            $response['message'] = 'Error sending email. please try again.';
        }
        return $response;
    }

    public function validate_reset_token($where_data) {
        if ($result = $this->db->get_where("users", ['reset_token' => $where_data['reset_token']])) {
            $data = $result->first_row('array');
            if (!empty($data)) {
                $response['email_id'] = $data['email_id'];
                $response['user_id'] = $data['user_id'];
                return $response;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
    
    public function reset_save_password($reset_token,$password) {
        $this->db->where("reset_token",$reset_token);
        if($result = $this->db->get("users")){
            $user_data = $result->first_row("array");
        $sql = "UPDATE users SET password = AES_ENCRYPT('".$password."','".AES_ENCRYPT_KEY."') WHERE user_id='".$user_data['user_id']."'";
        if($this->db->query($sql)){
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
        }
        
    }

}
