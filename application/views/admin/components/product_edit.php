<div class="modal fade" id="modalEditProduct" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Edit Product</h4>
            </div>
            <div class="modal-body">
                <form id="formEditProduct" onsubmit="return false;">
                    <input type="hidden" id="productId" name="productId" value="<?= $id; ?>">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?= $productData['name'] ?>" required="" data-msg="Please provide a product name.">
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <select class="form-control" id="category_id" name="category_id" required data-msg="Please select a category.">
                            <option value="">Please Select</option>
                            <?php foreach ($categoryList as $key => $category) { ?>
                            <option value="<?= $category['id'] ?>" <?= ($category['id']==$productData['category_id'])?"selected":""; ?>><?= $category['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Price</label>
                        <input type="text" class="form-control" id="price" name="price" placeholder="Price" value="<?= $productData['price'] ?>" required data-msg="Please enter a price." data-rule-number="true" data-msg-number="Please enter a valid price.">
                    </div>
                    <div class="form-group">
                        <label for="name" style="padding-right: 10px;">In Stock</label>
                        <input type="checkbox" id="in_stock" name="in_stock" value="1" <?= ($productData['in_stock']=='1')?"checked":""; ?>>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="updateProduct();">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>