<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <?php $this->load->view("customer/layout/header_view"); ?>
    <body>
        <?php $this->load->view("customer/layout/header_content_view"); ?>

        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Login/Register</h1>
                        <nav class="d-flex align-items-center">
                            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="category.html">Login/Register</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Login Box Area =================-->
        <section class="login_box_area section_gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="login_box_img">
                            <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/login.jpg" alt="">
                            <div class="hover">
                                <h4>New to our website?</h4>
                                <p>There are advances being made in science and technology everyday, and a good example of this is the</p>
                                <a class="primary-btn" href="registration.html">Create an Account</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="login_form_inner">
                            <h3>Log in to enter</h3>
                            <form class="row login_form" id="formLogin" onsubmit="return false;">
                                <div class="col-md-12 form-group">
                                    <input type="text" class="form-control" id="username" name="username" required="" data-msg="Please enter your email-id" placeholder="Username" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Username'">
                                </div>
                                <div class="col-md-12 form-group">
                                    <input type="password" class="form-control" id="password" name="password" required="" data-msg="Please enter your password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                                </div>
                                <div class="col-md-12 form-group">
                                    <button type="submit" value="submit" id="btnLogin" onclick="validateLogin();" class="primary-btn">Log In</button>
                                </div>
                                <div id="loginResponseMessage" style="padding-left: 15px;"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Login Box Area =================-->

        <?php $this->load->view("customer/layout/footer_content_view"); ?>
        <?php $this->load->view("customer/layout/footer_view"); ?>
        <script src="<?= base_url('assets/admin/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
    </body>

</html>