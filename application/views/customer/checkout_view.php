<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <?php $this->load->view("customer/layout/header_view"); ?>
    <body>
        <?php $this->load->view("customer/layout/header_content_view"); ?>

        <section class="checkout_area section_gap" style="padding-top: 200px;">
            <div class="container">
                <?php if (empty($_SESSION['SESSION_KEY'])) { ?>
                    <div class="returning_customer">
                        <div class="check_title">
                            <h2>Returning Customer? <a href="<?= base_url("login"); ?>">Click here to login</a></h2>
                        </div>
                    </div>                    
                <?php } else { ?>

                    <div class="billing_details">
                        <div class="row">
                            <div class="col-lg-8">
                                <h3>Billing Details</h3>
                                <form class="row contact_form" id="formProductCheckout" name="formProductCheckout" onsubmit="return false;">
                                    <input type="hidden" id="mode_of_payment" name="mode_of_payment" value="C">
                                    <div class="col-md-6 form-group p_star">
                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First name" required data-msg="Please enter the first name.">
                                    </div>
                                    <div class="col-md-6 form-group p_star">
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last name" required data-msg="Please enter the last name.">
                                    </div>
                                    <div class="col-md-6 form-group p_star">
                                        <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone number" required data-msg="Please enter the phone number.">
                                    </div>
                                    <div class="col-md-6 form-group p_star">
                                        <input type="text" class="form-control" id="email_id" name="email_id" placeholder="Email Address" required data-msg="Please enter the email id.">
                                    </div>
                                    <div class="col-md-12 form-group p_star">
                                        <input type="text" class="form-control" id="address" name="address" placeholder="Address line" required data-msg="Please enter the address.">
                                    </div>
                                    <div class="col-md-12 form-group p_star">
                                        <input type="text" class="form-control" id="city" name="city" placeholder="Town/City" required data-msg="Please enter the city.">
                                    </div>
                                    <div class="col-md-12 form-group p_star">
                                        <input type="text" class="form-control" id="district" name="district" placeholder="District" required data-msg="Please enter the district.">
                                    </div>
                                </form>
                                <div id="checkoutResponse" class="alert"> </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="order_box">
                                    <h2>Your Order</h2>
                                    <ul class="list">
                                        <li><a href="#">Product <span>Total</span></a></li>
                                        <?php
                                        $checkoutTotal = 0;
                                        foreach ($checkoutData as $key => $product) {
                                            $productTotal = floatval($product['price']) * floatval($product['quantity']);
                                            $checkoutTotal += $productTotal;
                                            ?>
                                            <li><a href="#"><?= $product['name'] ?> <span class="middle">x <?= $product['quantity']; ?></span> <span class="last">$<?= $productTotal; ?></span></a></li>
                                        <?php } ?>
                                    </ul>
                                    <ul class="list list_2">
                                        <li><a href="#">Subtotal <span>$<?= number_format($checkoutTotal, 2); ?></span></a></li>
                                        <li><a href="#">Total <span>$<?= number_format(floatval($checkoutTotal), 2); ?></span></a></li>
                                    </ul>
                                    <div class="payment_item">
                                        <div class="radion_btn">
                                            <input type="radio" class="paymentOption" value="C" name="paymentOption" id="f-option5" checked>
                                            <label for="f-option5">Cash On Delivery</label>
                                            <div class="check"></div>
                                        </div>
                                    </div>
                                    <div class="payment_item">
                                        <div class="radion_btn">
                                            <input type="radio" class="paymentOption" value="R" name="paymentOption" id="f-option6">
                                            <label for="f-option6">Credit Card</label>
                                            <div class="check"></div>
                                        </div>
                                    </div>
                                    <div class="payment_item">
                                        <div class="radion_btn">
                                            <input type="radio" class="paymentOption" value="D" name="paymentOption" id="f-option7">
                                            <label for="f-option7">Debit Card</label>
                                            <div class="check"></div>
                                        </div>
                                    </div>                                
                                    <a class="primary-btn" id="btnPlaceOrder" href="javascript:void(0);" onclick="confirmOrder();">Place Order</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </section>

        <?php $this->load->view("customer/layout/footer_content_view"); ?>
        <?php $this->load->view("customer/layout/footer_view"); ?>
    </body>
    <script>
        $(".paymentOption").change(function () {
            $("#mode_of_payment").val($(this).val());
        });
        function confirmOrder() {
            $("#formProductCheckout").validate();
            if ($("#formProductCheckout").valid()) {
                $("#btnPlaceOrder").attr('onclick','');
                $.post($("#hid_site_url").val() + "customer/checkout/confirmCheckout", $("#formProductCheckout").serialize(), function (response) {
                    if (response.status == 'SUCCESS') {
                        $("#checkoutResponse").addClass("alert-success").html("Order placed Successfully.");
                        localStorage.removeItem("userCart");
                        setTimeout(function () {
                            window.location.href = $("#hid_site_url").val();
                        }, 5000);
                    } else {
                        $("#btnPlaceOrder").attr('onclick','confirmOrder()');
                        $("#checkoutResponse").addClass("alert-success").html("Something went wrong. Please try again later.");
                    }
                });
            }
        }
    </script>
</html>