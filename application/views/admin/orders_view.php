<!DOCTYPE html>
<html>
    <?php $this->load->view('admin/layout/header'); ?>
    <body class="hold-transition skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <?php $this->load->view('admin/layout/header-content'); ?>
            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <?php $this->load->view('admin/layout/sidebar'); ?>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Orders
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Orders</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Orders</h3>
                        </div>
                        <div class="box-body">
                            <div id="dataContent"></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <?php $this->load->view('admin/layout/footer-content'); ?>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <?php $this->load->view('admin/layout/footer'); ?>
        <div id="modalContainer"></div>
    </body>
    <script>

        $(document).ready(function () {
            ordersList();
        });

        function ordersList() {
            $.post($("#hid_site_url").val() + "admin/order/getorderlist", function (response) {
                $("#dataContent").html(response);
            });
        }

        function viewOrderSummary(orderId){
            $.post($("#hid_site_url").val() + "admin/order/getOrderSummary", {orderId: orderId}, function (response) {
                $("#modalContainer").html(response);
                $("#modalOrderSummary").modal("show");
            });
        }
    </script>
</html>
