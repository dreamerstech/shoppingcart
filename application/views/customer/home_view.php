<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <?php $this->load->view("customer/layout/header_view"); ?>
    <body>
        <?php $this->load->view("customer/layout/header_content_view"); ?>
        <style>
            .active-product-area{
                padding-top: 200px;
            }
        </style>
        <!-- start product Area -->
        <section class="active-product-area section_gap">
            <!-- single product slide -->
            <div class="single-product-slider">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <div class="section-title">
                                <h1>Categories</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- single product -->
                        <?php foreach ($categoryList as $key => $category) { ?>
                            <div class="col-lg-3 col-md-6">
                                <a href="<?= base_url("products/").$category['id']; ?>">
                                    <div class="single-product">
                                        <img class="img-fluid" src="<?= base_url('assets/customer/'); ?>img/product/p1.jpg" alt="">
                                        <div class="product-details">
                                            <h6><?= $category['name']; ?></h6>
                                        </div>
                                    </div>
                                </a>
                            </div>                            
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- end product Area -->


        <?php $this->load->view("customer/layout/footer_content_view"); ?>
        <?php $this->load->view("customer/layout/footer_view"); ?>
    </body>

</html>