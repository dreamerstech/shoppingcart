<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard_Model extends CI_Model {

    public function summary() {
        $data['categoryCount'] = $data['productCount'] = $data['ordersCount'] = 0;
        if ($result = $this->db->query("SELECT COUNT(id) as category_count FROM master_category WHERE is_active=1")) {
            $data['categoryCount'] = $result->result('array')[0]["category_count"];
        }
        if ($result = $this->db->query("SELECT COUNT(id) as product_count FROM master_product WHERE in_stock=1")) {
            $data['productCount'] = $result->result('array')[0]["product_count"];
        }
        if ($result = $this->db->query("SELECT COUNT(id) as orderCount FROM orders WHERE status !='C'")) {
            $data['ordersCount'] = $result->result('array')[0]["orderCount"];
        }
        return $data;
    }

}
