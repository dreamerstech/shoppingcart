<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Order_Model extends CI_Model {

    public function getOrderList($wheredata = []) {
        if ($result = $this->db->query("SELECT o.id AS orderId, o.status,GROUP_CONCAT(p.name) AS product_names,u.name as username,o.mode_of_payment FROM ordered_products op INNER JOIN orders o ON o.id=op.order_id INNER JOIN users u ON u.id=o.user_id INNER JOIN master_product p ON p.id=op.product_id GROUP BY o.id ASC")) {
            return $result->result('array');
        } else {
            return [];
        }
    }
    
    public function getOrderSummary($orderId) {
        $orderData = [];
        $sql = "SELECT o.mode_of_payment, o.status, b.first_name,b.last_name,b.phone_number,b.email_id,b.address,b.city,b.district FROM orders o INNER JOIN user_billing_address b ON o.id=b.order_id WHERE o.id=?";
        if($result = $this->db->query($sql,$orderId)){
            $orderData = $result->result('array')[0];
            $sql = "SELECT p.name AS product_name,op.quantity,op.price FROM ordered_products op INNER JOIN master_product p ON p.id=op.product_id WHERE op.order_id=?";
            if($productresult = $this->db->query($sql,$orderId)){
                $orderData['productData'] = $productresult->result('array');
            }
        }
        return $orderData;
    }

    public function checkoutDetails($selectedProducts) {
        $productsArr = [];
        foreach ($selectedProducts as $key => $products) {
            array_push($productsArr, $products->id);
        }
        $sql = "SELECT id,name,price FROM master_product WHERE id IN (" . implode(",", $productsArr) . ")";
        if ($result = $this->db->query($sql)) {
            $resultData = $result->result('array');
            if (count($resultData) != 0) {
                foreach ($resultData as $key => $data) {
                    $resultData[$key]['quantity'] = $this->getQuantity($data['id'], $selectedProducts);
                }
            }
            return $resultData;
        } else {
            return [];
        }
    }

    public function addCustomerOrder($orderData, $mode_of_payment, $billingData) {
        $insertOrder['user_id'] = $_SESSION['user_id'];
        $insertOrder['mode_of_payment'] = $mode_of_payment;
        $insertOrder['order_placed_at'] = gmdate("Y-m-d H:i:s");
        if ($this->db->insert("orders", $insertOrder)) {
            $orderId = $this->db->insert_id();
            foreach ($orderData as $key => $product) {
                $orderedProduct['order_id'] = $orderId;
                $orderedProduct['product_id'] = $product['id'];
                $orderedProduct['quantity'] = $product['quantity'];
                $orderedProduct['price'] = $product['price'];
                $this->db->insert("ordered_products", $orderedProduct);
                unset($orderedProduct);
            }
            $billingData['order_id'] = $orderId;
            if ($this->db->insert("user_billing_address", $billingData)) {
                return 'SUCCESS';
            } else {
                return 'FAIL';
            }
        } else {
            return 'FAIL';
        }
    }

    public function getQuantity($id, $selectedProducts) {
        foreach ($selectedProducts as $key => $products) {
            if ($id == $products->id) {
                return intval($products->quantity);
            }
        }
        return 1;
    }

}
