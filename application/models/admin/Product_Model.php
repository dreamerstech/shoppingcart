<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Product_Model extends CI_Model {

    public function getProductData($whereData = []) {
        $whereData['c.is_active'] = 1;
        $this->db->select("p.id,p.name,p.price,p.category_id,c.name as category_name,p.in_stock");
        $this->db->join("master_category c","c.id=p.category_id");
        if($result = $this->db->get_where("master_product p",$whereData)){
            return $result->result('array');
        } else {
            return [];
        }
    }
    
    public function addProduct($insertData) {
        if($this->db->insert("master_product",$insertData)){
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }
    
    public function updateProduct($updateData,$whereData) {
        if($this->db->update("master_product",$updateData,$whereData)){
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }
    
    public function deleteProduct($whereData) {
        if($this->db->delete("master_product",$whereData)){
            return 'SUCCESS';
        } else {
            return 'FAIL';
        }
    }
}
