<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index() {

        if (!empty($this->input->get("data"))) {
            $selectedProducts = json_decode(base64_decode($this->input->get("data"),true));
            
            $this->load->model("admin/Order_Model");
            $_SESSION['selectedProducts'] = $data['checkoutData'] = $this->Order_Model->checkoutDetails($selectedProducts);

            $this->load->view("customer/checkout_view",$data);
        } else {
            show_404();
        }
    }
    
    public function confirmCheckout() {
        $selectedProducts = $_SESSION['selectedProducts'];
        $response = [];
        $dataRule = array(
            array('field' => 'mode_of_payment', 'rules' => 'required'),
            array('field' => 'first_name', 'rules' => 'required'),
            array('field' => 'last_name', 'rules' => 'required'),
            array('field' => 'phone_number', 'rules' => 'required'),
            array('field' => 'email_id', 'rules' => 'required'),
            array('field' => 'address', 'rules' => 'required'),
            array('field' => 'city', 'rules' => 'required'),
            array('field' => 'district', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($dataRule);

        if ($this->form_validation->run() == TRUE) {
            $mode_of_payment = $this->input->post("mode_of_payment");
            $billingData['user_id'] = $_SESSION['user_id'];
            $billingData['first_name'] = $this->input->post("first_name");
            $billingData['last_name'] = $this->input->post("last_name");
            $billingData['phone_number'] = $this->input->post("phone_number");
            $billingData['email_id'] = $this->input->post("email_id");
            $billingData['address'] = $this->input->post("address");
            $billingData['city'] = $this->input->post("city");
            $billingData['district'] = $this->input->post("district");
            $billingData['created_ts'] = gmdate("Y-m-d H:i:s");
            
            //$billingData = $this->input->post();

            $this->load->model("admin/Order_Model");
            $response['status'] = $this->Order_Model->addCustomerOrder($selectedProducts,$mode_of_payment,$billingData);
        } else {
            $response['status'] = "FAIL";
        }
        header('Content-type: application/json');
        echo json_encode($response);
    }

}

?>