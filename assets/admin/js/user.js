

function change_user_password(){
    $("#form_change_user_password").validate();
    var isvalid = $("#form_change_user_password").valid();
    if(isvalid){
        $.post($("#hid_site_url").val()+"user/user_change_password",$("#form_change_user_password").serialize(),function(result){
            if(result.status == 'SUCCESS'){
                $.notify("Password changed successfully", "success");
                $("#form_change_user_password input").val('');
            }
        });
    }
}